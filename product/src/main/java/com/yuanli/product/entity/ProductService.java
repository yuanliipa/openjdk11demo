package com.yuanli.product.entity;


import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class ProductService {
    private String name;
    private Double price;
    private String type;
}
