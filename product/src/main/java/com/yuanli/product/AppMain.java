package com.yuanli.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Zhang Wei
 * @Create: 2018-11-12
 */
@SpringBootApplication
public class AppMain {
    public static void main(String[] args){
        SpringApplication.run(AppMain.class);
    }
}
