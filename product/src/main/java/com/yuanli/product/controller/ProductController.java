package com.yuanli.product.controller;

import com.yuanli.product.entity.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Zhang Wei
 * @Create: 2018-11-12
 */
@RestController
@RequestMapping("/api/products_service")
public class ProductController {

    static Map<String, ProductService> products = new HashMap();

    static{
        products.put("brand", ProductService.builder().name("商标").price(800d).type("brand").build());
        products.put("brandVip", ProductService.builder().name("商标-加急").price(1000d).type("brand").build());
        products.put("copyright", ProductService.builder().name("版权").price(600d).type("copyright").build());
        products.put("invention", ProductService.builder().name("专利-发明").price(6500d).type("patent").build());
        products.put("facade", ProductService.builder().name("专利-外观").price(680d).type("patent").build());
        products.put("utility", ProductService.builder().name("专利-实用新型").price(680d).type("patent").build());
    }


    @GetMapping("products")
    public ResponseEntity<Object> products(){
        return new ResponseEntity(products ,HttpStatus.OK);
    }

    @GetMapping("product/{name}")
    public ResponseEntity<ProductService> retrieveProduct(@PathVariable String name){
        return new ResponseEntity(products.get(name) ,HttpStatus.OK);
    }

    @PutMapping("product/{name}")
    public ResponseEntity<ProductService> updateProduct(@PathVariable String name, @RequestBody  ProductService value){
        products.put(name, value);
        return new ResponseEntity(products.get(name) ,HttpStatus.OK);
    }

    @PostMapping("product/{name}")
    public ResponseEntity<ProductService> createProduct(@PathVariable String name, @RequestBody  ProductService value){
        products.put(name, value);
        return new ResponseEntity(products.get(name) ,HttpStatus.OK);
    }

    @DeleteMapping("product/{name}")
    public ResponseEntity<Object> removeProduct(@PathVariable String name){
        products.remove(name);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<Object> removeProducts(){
        products.clear();
        return new ResponseEntity(HttpStatus.OK);
    }

}
